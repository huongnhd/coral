'use strict';

let host;

if(process.env.NODE_ENV == "test"|| process.env.NODE_ENV == "dev" ){
  host = "http://localhost:4000";
}else{
  host = location.origin;
}


module.exports = {
  API_AUTH: {
    host: 'http://localhost:4000',
    baseUri: host + '/api/v1/',
    auth: 'auth'
  },
  API_SOL: {
    host: 'http://localhost:4000',
    baseUri: host + '/api/v1/',
    users: 'users',
    courses: 'products',
    course_rate: 'product_factors',
    course_categories: 'course_categories'
  },
  API_HERA: {
    host: 'http://localhost:4000',
    baseUri: host + '/api/v1/',
    landing_pages: 'landing_pages'
  },
  API_NAMI: {
    host: 'http://localhost:4000',
    baseUri: host + '/api/v1/',
    facebook_pages: 'facebook_pages'
  }
};