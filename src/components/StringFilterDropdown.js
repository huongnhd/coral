import React, { Component, PropTypes } from 'react';
import { Input } from 'antd';

export default class StringFilterDropdown extends Component {
  static propTypes = {
    columnKey: PropTypes.string.isRequired
  };

  static contextTypes = {
    handleCustomFilter: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.handleClearFilters = this.handleClearFilters.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      inputFilter: ''
    };
  }

  // TODO: Because ant-design filterDropdown does not provide an interface for setting whether dropdown is displayed, you need to increase the ability to automatically disable dropdown.
  handleConfirm(){
    const { columnKey } = this.props;
    const filters = { [columnKey]: this.state.inputFilter };
    this.context.handleCustomFilter(columnKey, filters);
  }

  handleInputChange(event){
    this.setState({ inputFilter: event.target.value });
  }

  // TODO: Because ant-design filterDropdown does not provide an interface for setting whether dropdown is displayed, you need to increase the ability to automatically disable dropdown.
  handleClearFilters() {
    const { columnKey } = this.props;
    this.setState({ inputFilter: ''});
    this.context.handleCustomFilter(columnKey, {[columnKey]: []});
  }

  render() {
    return (
      <div className="ant-table-filter-dropdown">
        <Input
          placeholder="Please enter query conditions"
          value={this.state.inputFilter}
          onChange={this.handleInputChange}
          style={{ width: 184, margin: "10px" }}
        />
        <div className="ant-table-filter-dropdown-btns">
          <a
            className="ant-table-filter-dropdown-link confirm"
            onClick={this.handleConfirm}
          >
            confirm
          </a>
          <a
            className="ant-table-filter-dropdown-link clear"
            onClick={this.handleClearFilters}
          >
            Reset
          </a>
        </div>
      </div>
    );
  }
}
