'use strict';

import { Breadcrumb } from 'antd';
import PropTypes from 'prop-types';
import React from 'react';

const BreadcrumbWithPath = ({ path }) => {
    let breadcrumbs = [];
    breadcrumbs.push("Home");
    if (path != null || path != undefined) {
        let pathNames = path.split('/').filter(p => p != '');
        if (pathNames.length > 0) {
           breadcrumbs.push(...pathNames);
        }
    }


    const breadcrumbItem = breadcrumbs.map(breadcrumb => <Breadcrumb.Item key={breadcrumb + "_breadcrumb"} >{breadcrumb}</Breadcrumb.Item>);

    return (
        <Breadcrumb>
            { breadcrumbItem }
        </Breadcrumb>

    );
};
BreadcrumbWithPath.propTypes = {
    path: PropTypes.string.isRequired
};
export default BreadcrumbWithPath;