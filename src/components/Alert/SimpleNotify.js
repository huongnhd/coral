'use strict';

import { notification } from 'antd';

const simpleNotify = (tittle, message) => {
    notification.open({
        message: tittle,
        description: message
    });
};
export default simpleNotify;