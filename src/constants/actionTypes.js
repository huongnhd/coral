// here is a unified action constant

// auth.js
export const USER = 'USER';
export const LOGIN = 'LOGIN';

export const CURRENT_USER_REQUEST = 'CURRENT_USER_REQUEST';
export const CURRENT_USER_SUCCESS = 'CURRENT_USER_SUCCESS';

export const COURSES = 'COURSES';
export const COURSE_INFO = 'COURSES';
export const COURSES_CATEGORY = 'COURSES_CATEGORY';

export const COURSE_RATES = 'COURSE_RATES';
export const CREATE_COURSE_RATE = 'CREATE_COURSE_RATE';
export const UPDATE_COURSE_RATE = 'UPDATE_COURSE_RATE';
export const DELETE_COURSE_RATE = 'DELETE_COURSE_RATE';


// Landing pages
export const LIST_LANDING_PAGES  = 'LIST_LANDING_PAGES';
export const DETAIL_LANDING_PAGE = 'DETAIL_LANDING_PAGE';
export const UPDATE_LANDING_PAGE = 'UPDATE_LANDING_PAGE';

// facebook-pages
export const LIST_FACEBOOK_PAGES = 'LIST_FACEBOOK_PAGES';