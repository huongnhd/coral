'use strict';

/* Container is show course category 
 * TODO: This is data static (or master data) . So we need storing this to catche instead of calling API 
 */

import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { Select } from 'antd';
import { connect } from 'react-redux';
import { fetchCourseCategories } from '../../actions/course';

const Option = Select.Option;

class CourseCategorySelect extends Component {

    constructor(props) {
        super(props);
        this.value = props.value;
        this.onChange = props.onChange.bind(this);
    }

    componentDidMount() {
        this.props.fetchCourseCategories();
    }

    render() {
        const CategoryItems =
            this.props
                .courseCategories.map(
                    category => (
                        <Option
                            style={{ flex: 1 }}
                            key={category.name}
                            value={category.name}
                        >
                            {category.name}
                        </Option>
                    )
                );

        return (
            <Select showSearch
                defaultValue={this.value}
                style={{ width: 200 }}
                placeholder="Select a the category course "
                optionFilterProp="children"
                onChange={this.onChange}
            >
                {CategoryItems}

            </Select>
        );

    }
}

const mapStateToProps = state => ({
    courseCategories: state.courseCategories.records
});

CourseCategorySelect.propTypes = {
    courseCategories: PropTypes.array.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
};


export default connect( mapStateToProps, { fetchCourseCategories })(CourseCategorySelect);
