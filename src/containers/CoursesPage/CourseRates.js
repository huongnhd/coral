import { Divider, Icon, Row, Tabs } from 'antd';
import React, { Component } from 'react';

import CourseRateTable from './CourseRateTable';
import PropTypes from 'prop-types';
import WrapperCourseRateForm from './CourseRateForm';

const TabPane = Tabs.TabPane;

class CourseRates extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className="course-card" style={{ padding: 10 }}>
                <Tabs defaultActiveKey="1" >
                    <TabPane tab={<span><Icon type="eye" />Hệ số tổng </span>} key="1">
                        <Row>
                            <div className="course-card" style={{ padding: 16 }}>
                                <WrapperCourseRateForm courseId={this.props.courseId} />
                            </div>
                        </Row>
                        <Divider />
                        <Row>
                            <CourseRateTable courseId={this.props.courseId} />
                        </Row>
                    </TabPane>
                    <TabPane tab={<span><Icon type="eye" />Thông tin cơ bản</span>} key="2">
                        TODO:// Update letter
                    </TabPane>
                </Tabs>
            </div>
        );
    }
}


CourseRates.propTypes = {
    courseId: PropTypes.number.isRequired,
};

export default CourseRates;