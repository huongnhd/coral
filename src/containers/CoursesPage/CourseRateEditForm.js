'use strict';

import { Button, Col, DatePicker, Form, InputNumber, Row } from 'antd';

import CourseCategorySelect from './CourseCategorySelect';
import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import { simpleNotify } from '../../components/Alert';
import { updateCourseRate } from '../../actions/courseRate';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';

/* Component CourseRateEditForm only using for update the course rate or (factor of course) 
 */
class CourseRateEditForm extends React.Component {
    constructor(props) {
        super(props);
        _.bindAll(this, ['_handleSubmit']);
        this.state = {showAlert: false};
    }

    _handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const newCourseRate = {
                    course_id: this.props.item.course_id,
                    start_date: values.date_range[0].format(dateFormat).toString(),
                    end_date: values.date_range[1].format(dateFormat).toString(),
                    category_name: values.category_name,
                    total_rate: values.total_rate
                };
                let item = Object.assign(this.props.item, newCourseRate);
                this.props.updateCourseRate(item);
                simpleNotify("Success", "Update sucessfully!");

            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        // TODO: need write validator data  not same  previous data 
        return (
            <Form onSubmit={this._handleSubmit} title="Add course rate">
               
                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8} >
                        Start Date - End Date
                </Col >
                    <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <FormItem>
                            {getFieldDecorator(['date_range'], {
                                rules: [{ required: true, message: 'Please input Range date!', type: 'array' }],
                                initialValue: [moment(this.props.item.start_date, dateFormat), moment(this.props.item.end_date, dateFormat)],
                                transform: (v => v.map(v => v.format(dateFormat).toString()))
                            })(
                                <RangePicker
                                    format={dateFormat}
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8}> Category</Col>
                    <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <FormItem>
                            {getFieldDecorator('category_name', {
                                rules: [
                                    { required: true, message: 'Please select category for course!' }
                                ],
                                initialValue: this.props.item.category_name,

                            })(
                                <CourseCategorySelect />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8}> Factor</Col>
                    <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <FormItem>
                            {getFieldDecorator('total_rate', {
                                rules: [{ required: true, message: 'Please input factor !' }],
                                initialValue: parseFloat(this.props.item.total_rate)
                            })(
                                <InputNumber />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={4} lg={4} xl={4}>  <FormItem>
                        <Button type="primary" htmlType="submit">
                            Submit
                    </Button>
                    </FormItem></Col>
                    <Col xs={24} sm={24} md={20} lg={20} xl={20} />

                </Row>
            </Form>
        );
    }
}

CourseRateEditForm.propTypes = {
    item: PropTypes.object.isRequired,
    updateCourseRate: PropTypes.func.isRequired,
    courseRates: PropTypes.object,
    onClose: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        courseRates: state.courseRates
    };
};

const WrapperEditCourseRateForm = Form.create()(CourseRateEditForm);


export default connect(mapStateToProps, { updateCourseRate })(WrapperEditCourseRateForm);