import { Button, Divider, Modal, Popconfirm, Table } from 'antd';
/* Course Rate table cant edit row and delete row 
 If create then update table
*/
import React, { Component } from 'react';
import { deleteCourseRate, fetchCourseRates, updateCourseRate } from '../../actions/courseRate';

import PropTypes from 'prop-types';
import WrapperEditCourseRateForm from './CourseRateEditForm';
import _ from 'lodash';
import { connect } from 'react-redux';
import { simpleNotify } from '../../components/Alert';

class CourseRateTable extends Component {
    constructor(props) {
        super(props);
        this.state = { visible: false, item: {} };

        this.columns = [
            {
                title: 'Start date',
                dataIndex: 'start_date',
                key: 'start_date',
            }, {
                title: 'End date',
                dataIndex: 'end_date',
                key: 'end_date',
            }, {
                title: 'Rate',
                dataIndex: 'total_rate',
                key: 'total_rate',
            }, {
                title: "Category",
                dataIndex: 'category_name',
                key: 'category_name'
            }, {
                title: "User",
                dataIndex: 'user_username',
                key: 'user_username'

            }, {
                title: 'Action',
                key: 'id',
                render: (text, record) =>
                    (
                        <span>
                            <Button type="success" data={record} size="small" onClick={() => this.openModalEdit(record)}>Edit</Button>
                            <Divider type="vertical" />

                            <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record.id)}>
                                <Button type="danger" data={record} size="small">Delete</Button>
                            </Popconfirm>
                        </span>
                    )
            }
        ];

        _.bindAll(this, [
            'openModalEdit',
            'offModalEdit',
            'handleDelete'
        ]);

    }


    componentDidMount() {
        this.props.fetchCourseRates(this.props.courseId);
    }



    handleDelete(id) {
        this.props.deleteCourseRate(id);
        this.props.fetchCourseRates(this.props.courseId);
        simpleNotify("Delete action", "success");
    }

    openModalEdit(item) {
        //Set state item, set state visible modal
        this.setState({ item: item, visible: true });
    }

    offModalEdit() {
        this.setState({ visible: false, item: {} });
    }


    render() {
        const dataSource = this.props.courseRates.records;
        return (
            <div className="course-card" style={{ padding: 10 }}>
                <Modal
                    title="Edit course rate"
                    visible={this.state.visible}
                    onCancel={this.offModalEdit}
                    afterClose={this.offModalEdit}
                    footer={[<Button key="back" onClick={this.offModalEdit}>Return</Button>,]}
                >
                    <WrapperEditCourseRateForm item={this.state.item} />
                </Modal>

                <Table
                    dataSource={dataSource}
                    columns={this.columns}
                    rowKey={(r, i) => i}
                    size="small"
                />


            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        courseRates: state.courseRates,
    };
};

CourseRateTable.propTypes = {
    fetchCourseRates: PropTypes.func.isRequired,
    deleteCourseRate: PropTypes.func.isRequired,
    updateCourseRate: PropTypes.func.isRequired,
    courseId: PropTypes.number.isRequired,
    courseRates: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, { deleteCourseRate, updateCourseRate, fetchCourseRates })(CourseRateTable);