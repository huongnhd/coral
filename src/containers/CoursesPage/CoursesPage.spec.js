// This is test case of Course page

import chai, { expect } from 'chai';
import sinon from "sinon";
import sinonChai from "sinon-chai";

import React from 'react';
import { mount, shallow } from 'enzyme';
import { CoursesPage } from './CoursesPage';
import { Col } from 'antd';

const db = require('./../../data/db.json');

chai.use(sinonChai);

function setup() {
  const props = {
    fetchCourses: sinon.spy(),
    courses: {
      isFetching: false,
      error: '',          
      records: db.products.records,
      filters: {
        paging: {
          page: db.products.filters.paging.page,
          per_page: db.products.filters.paging.per_page,
          record_total: db.products.filters.paging.record_total,
        }}
    }
  };

  const enzymeWrapper = shallow(<CoursesPage {...props} />);
  const mountWrapper = mount(<CoursesPage {...props} />);

  return {
    props,
    enzymeWrapper,
    mountWrapper
  };
}

describe('CoursePage container', () => {
  it('should render self with props', () => {
    // componentDidMount
    const { mountWrapper, props } = setup();
    expect(props.fetchCourses).to.have.been.calledOnce;

    // simulates click to trigger handleTableChange with page 2
    mountWrapper.find('.ant-pagination-item-2').simulate('click');
    expect(props.fetchCourses).to.have.been.calledWith();
  });

  it('should render two <Col /> components', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Col)).to.have.length(2);
  });

  it('should render <CustomTable /> components and props', () => {
    const { enzymeWrapper } = setup();
    const tableProps = enzymeWrapper.find('CustomTable').props();
    expect(tableProps.dataSource).to.deep.equal(db.courses.records);
  });
});


