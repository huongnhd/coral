import { Button, Col, Divider, Input, Row, Table } from 'antd';

import React from 'react';
import StringFilterDropdown from './../../components/StringFilterDropdown';
import { connect } from 'react-redux';
import { fetchCourses } from '../../actions/course';

const InputGroup = Input.Group;

function mapStateToProps(state) {
  const { courses } = state;
  return {
    courses
  };
}

export class CoursesPage extends React.Component {
  static propTypes = {
    fetchCourses: React.PropTypes.func,
    courses: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    this.handleTableChange = this.handleTableChange.bind(this);
    this.state = {
      selectedRowKeys: []
    };
  }

  componentDidMount() {
    this.props.fetchCourses();
  }


  handleTableChange(pagination, filters = {}, sorter = {}) {
  
    // page=1&orders[]=id.desc&full_search=
    // TODO:// check handleTableChange with search textbox, and sort column
    const pageParams = { page: pagination.current };
    const fullSearch = {full_search: this.refs.searchBox.value};
    const filtersField = {};
    if (Object.keys(filters).length !== 0) {
      // enum filters
      [{
        key: "category_name", filterParams: "category_name_in"
      }].map(item => {
        if (filters[item.key]) {
          filtersField[`q[${item.filterParams}]`] = filters[item.key];
        }
      });


      // string filter
      ['name'].map(item => {
        if (filters[item]) {
          filtersField[`q[${item}_cont]`] = filters[item];
        }
      });
    }
    const sortParams = {};
    if (Object.keys(sorter).length !== 0) {
      const sortMethod = sorter.order === "descend" ? "desc" : "asc";
      sortParams['orders'] = `${sorter.columnKey} ${sortMethod}`;
    }

    const params = Object.assign({}, pageParams, sortParams, filtersField, fullSearch);
    this.props.fetchCourses({ params });

  }

  render() {
    let pathOrigin = this.props.location.pathname;
    if (pathOrigin.endsWith('/')){
      pathOrigin = pathOrigin.substring(0,pathOrigin.length -1);
    }
    const { courses: { records, filters, isFetching } } = this.props;

    const columns = [
      {
        title: "ID",
        dataIndex: "id",
        key: "id",
        sorter: true
      },
      {
        title: "Mã khóa học",
        dataIndex: "code",
        key: "code",
        sorter: true,
        filterDropdown: <StringFilterDropdown columnKey={"code"} />
      }, {
        title: "Tên khóa học",
        dataIndex: "name",
        key: "name",
        sorter: true,
        filterDropdown: <StringFilterDropdown columnKey={"name"} />
      },
      {
        title: "	Loại khóa",
        dataIndex: "category_name",
        key: "category_name",
        sorter: true,
        filterDropdown: <StringFilterDropdown columnKey={"category_name_in"} />
      },
      {
        title: "Hệ số",
        dataIndex: "factor",
        key: "factor",
        sorter: true
      },
      {
        title: "Action",
        key: "tool",
        render: (record) => (
          <span>
            <Button type="success" data={record} size="small"
              onClick={() => this.props.history.push(pathOrigin + '/' + record.id)}
            >show</Button>
          </span>
        )
      }
    ];

    const pagination = {
      showSizeChanger: true,
      total: filters.paging.record_total,
      pageSize: filters.paging.per_page,
      pageSizeOptions: ['1', '10', '20', '40']
    };


    return (
      <div>
        <Row>
          <Col span={8}>
            <div>
              <h3>Course Management </h3>
            </div>
          </Col>
          <Col span={8} offset={8}>
            <div>
              <InputGroup className="ant-search-input">
                <Input placeholder="Search code, name" ref="searchBox" onChange={this.handleTableChange}/>
                <div className="ant-input-group-wrap">
                  <Button icon="search" className="ant-search-btn" />
                </div>
              </InputGroup>
            </div>
          </Col>
        </Row>
        <Divider />
        <Table
          columns={columns}
          dataSource={records}
          pagination={pagination}
          rowKey={(record) => record.code}
          loading={isFetching}
          onChange={this.handleTableChange}
          size="small"
          bordered
        />
      </div>
    );
  }
}


export default connect(
  mapStateToProps,
  { fetchCourses }
)(CoursesPage);
