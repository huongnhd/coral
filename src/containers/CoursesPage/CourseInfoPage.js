'use strict';

import { Col, Row } from 'antd';

import CourseCard from './CourseCard';
import CourseRates from './CourseRates';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { fetchCourseInfo } from '../../actions/course';

export class CourseInfoPage extends React.Component {
  static propTypes = {
    fetchCourseInfo: React.PropTypes.func,
    course: React.PropTypes.object
  };

  constructor(props) {
    super(props);
    }

  componentDidMount() {
    this.props.fetchCourseInfo(this.props.params.id);
  }

  render() {
    const { course: { record } } = this.props;
    return (
      <div>
        <Row gutter={16}>
          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
            <CourseCard course={record}  />
          </Col>
          <Col xs={24} sm={24} md={16} lg={16} xl={16}>
            <CourseRates courseId={921}/>
          </Col>
        </Row>

      </div>
    );
  }
}


function mapStateToProps(state) {
  const { course } = state;
  return { course };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchCourseInfo: (id) => dispatch(fetchCourseInfo(id))
  };
}

CourseInfoPage.PropTypes = {
  params: {
    id: PropTypes.string
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CourseInfoPage);
