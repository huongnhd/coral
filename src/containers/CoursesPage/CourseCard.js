import React, { Component } from 'react';

import { Card } from 'antd';
import PropTypes from 'prop-types';

class CourseCard extends Component {
    constructor(props) {
        super(props);
        this.course = props.course;
    }
    render() {
        return (
            <div className="course-card">
                <Card title={this.props.course.code}>
                    <li>Name: {this.props.course.name}</li>
                    <li>Factor: {this.props.course.factor}</li>
                    <li>Category: {this.props.course.category_name}</li>
                    <li>Last updated:  {this.props.course.updated_at}</li>
                </Card>
            </div>
        );
    }
}

CourseCard.propTypes = {
    course: PropTypes.object
};

export default CourseCard;
