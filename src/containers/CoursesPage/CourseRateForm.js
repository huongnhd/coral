'use strict';

import { Button, Col, DatePicker, Form, Row } from 'antd';

import CourseCategorySelect from './CourseCategorySelect';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { createCourseRate } from '../../actions/courseRate';

const FormItem = Form.Item;
const { RangePicker } = DatePicker;



class CourseRateForm extends React.Component {
    constructor(props) {
        super(props);
        this.courseId = this.props.courseId;
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const newCourseRate = {
                    course_id: this.courseId,
                    start_date: values.date_range[0].format("YYYY-MM-DD").toString(),
                    end_date: values.date_range[1].format("YYYY-MM-DD").toString(),
                    category_name: values.category_name
                };
                this.props.createCourseRate(newCourseRate);
            }
        });
    }

    onChange = (e) => {
        // console.log(e);
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form onSubmit={this.handleSubmit} title="Add course rate">
                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8} >
                        Start Date - End Date
                </Col >
                    <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <FormItem>
                            {getFieldDecorator(['date_range'], {
                                rules: [{ required: true, message: 'Please input Range date!', type: 'array' }],
                            })(
                                <RangePicker
                                    format="YYYY-MM-DD"
                                    onChange={this.onChange}
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8}> Category</Col>
                    <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                        <FormItem>
                            {getFieldDecorator('category_name', {
                                rules: [{ required: true, message: 'Please select category for course!' }],
                            })(
                                <CourseCategorySelect name={"category_name"} onChange={this.onChange} />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <Row style={{ marginTop: 16 }} >
                    <Col xs={24} sm={24} md={8} lg={8} xl={8}>  <FormItem>
                        <Button type="primary" htmlType="submit">
                            Submit
                    </Button>
                    </FormItem></Col>
                    <Col xs={24} sm={24} md={16} lg={16} xl={16} />

                </Row>
            </Form>
        );
    }
}

CourseRateForm.propTypes = {
    courseId: PropTypes.number.isRequired,
    createCourseRate: PropTypes.func
};

const WrapperCourseRateForm = Form.create()(CourseRateForm);

export default connect(null, { createCourseRate })(WrapperCourseRateForm);