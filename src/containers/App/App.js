import './App.scss';
import 'w3-css/w3.css';

import { Icon, LocaleProvider, Menu } from 'antd';
import React, { Component, PropTypes } from 'react';

import { Link } from 'react-router';
import Login from './../Login/Login';
import { connect } from 'react-redux';
import enUS from 'antd/lib/locale-provider/en_US';

const SubMenu = Menu.SubMenu;

class App extends Component {
  static propTypes = {
    children: PropTypes.element,
    isAuthenticated: React.PropTypes.bool,
    routing: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.renderAuthenticatedPage = this.renderAuthenticatedPage.bind(this);

    this.state = {
      collapse: true
    };

  }

  renderAuthenticatedPage() {
    return (

      <LocaleProvider locale={enUS}>
        <div className="ant-layout-aside">
          <aside className="ant-layout-sider">
            <div className="ant-layout-logo" />

            <Menu mode="inline" theme="dark"
              defaultSelectedKeys={['pricebook']} defaultOpenKeys={['pricebook']}>
              <SubMenu key="user" title={<span><Icon type="user" />User Management</span>}>
                <Menu.Item key="1">
                  <Link to={'/pricebook/users'}>
                    user list
                  </Link>
                </Menu.Item>
                <Menu.Item key="2">Role configuration</Menu.Item>
              </SubMenu>
              <SubMenu key="pricebook" title={<span><Icon type="profile" />Pricebook</span>}>
                <Menu.Item key="course">
                  <Link to={'/pricebook/courses'}>
                    Courses
                </Link>
                </Menu.Item>

                <Menu.Item key="langding-pages">
                  <Link to={'/pricebook/landing-pages'}>
                    Landing pages
                </Link>
                </Menu.Item>
              </SubMenu>
            </Menu>
          </aside>
          <div className="ant-layout-main">
        
            <div className="ant-layout-header" />
            
             {/* <div className="ant-layout-breadcrumb"> */}
              {/* <Breadcrumb>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Pricebook</Breadcrumb.Item>
                <Breadcrumb.Item>course</Breadcrumb.Item>
              </Breadcrumb> */}
            {/* </div>  */}
            <div className="ant-layout-container">
              <div className="ant-layout-content">
                {/* <div style={{ height: 590 }}> */}
                <div>
                  {this.props.children}
                </div>
              </div>
            </div>
            <div className="ant-layout-footer">
              Ant Design all rights reserved © 2015 Supported by Ant Financial Experience Technology Department
          </div>
          </div>
        </div>

      </LocaleProvider>
    );
  }

  render() {
    const { isAuthenticated } = this.props;
    return (
      <div>
        {isAuthenticated ? this.renderAuthenticatedPage() : <Login />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { routing, auth: { isAuthenticated, user } } = state;
  return {
    isAuthenticated, user, routing
  };
}

export default connect(mapStateToProps)(App);
