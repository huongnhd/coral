// Note: This component acts as an outer layer component for permission control and only renders data with permission
// Usage:
// <AccessControl ability="card_bindings_record">
//   <ul>
//   </ul>
// </AccessControl>
import { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

class AccessControl extends Component {

  canHandleChildCompoment() {
    const { user, ability } = this.props;
    if (user) {
      return user.accessible_features.indexOf(ability) > -1;
    }
    return false;
  }

  render() {
    if (this.canHandleChildCompoment()) {
      return this.props.children;
    }
    return null;
  }
}

AccessControl.propTypes = {
  ability: PropTypes.string.isRequired,
  user: PropTypes.object,
  children: PropTypes.element.isRequired
};

function mapStateToProps(state) {
  const { auth: { user } } = state;
  return { user };
}

export default connect(mapStateToProps)(AccessControl);
