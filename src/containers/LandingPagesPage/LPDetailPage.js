import { Col, Row, Spin } from 'antd';
import React, { Component } from 'react';

import { BreadcrumbWithPath } from '../../components/Breadcrumb';
import LPDetailCard  from './LPDetailCard';
import LPDetailCardEdit from './LPDetailCardEdit';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchCourses } from '../../actions/course';
import { fetchFacebookpages } from '../../actions/facebookpage';
import { fetchLandingPage } from '../../actions/landingpage';
import { getfullPath } from '../../utils/path';

class LPDetailPage extends Component {
  constructor(props) {
    super(props);

  }

  componentWillMount() {

  }

  componentDidMount() {
    this.props.fetchLandingPage(this.props.params.id);
    this.props.fetchCourses({ per_page: 'infinite' });
    this.props.fetchFacebookpages({ per_page: 'infinite' });
  }

  render() {
    /* init value of props avoid get properties of undefined var */
    const isLoading = this.props.landingPages.isFetching || this.props.courses.isFetching || this.props.facebookPages.isFetching;
    const landingPage = this.props.landingPages.record;
    const courses = this.props.courses.records;
    const facebookPages = this.props.facebookPages.records;

    return (
      <Spin spinning={isLoading}>
        <div>
          <Row>            
              <BreadcrumbWithPath path={getfullPath(this.props)} />
          </Row>
          
          <Row>
            <Col sm={24} md={24} lg={11} xl={11} xxl={11} >
              <LPDetailCard
              landingPage={landingPage} />
            </Col>
            <Col sm={24} md={24} lg= {{span: 12, offset: 1}} xl= {{span: 12, offset: 1}} xxl= {{span: 12, offset: 1}}>
              <LPDetailCardEdit courses={courses} facebookPages={facebookPages} landingPage={landingPage} />
            </Col>
          </Row>

        </div>
      </Spin>
    );
  }
}



LPDetailPage.propTypes = {
  landingPages: PropTypes.object.isRequired,
  fetchLandingPage: PropTypes.func.isRequired,

  courses: PropTypes.object.isRequired,
  fetchCourses: PropTypes.func.isRequired,

  facebookPages: PropTypes.object.isRequired,
  fetchFacebookpages: PropTypes.func.isRequired

};

const mapStateToProps = (state) => {
  return {
    landingPages: state.landingPages,
    courses: state.courses,
    facebookPages: state.facebookPages,
  };
};
export default connect(mapStateToProps, { fetchLandingPage, fetchCourses, fetchFacebookpages })(LPDetailPage);
