import React, { Component } from 'react';

import { BreadcrumbWithPath } from '../../components/Breadcrumb';
import LPListTable from './LPListTable';
import { getfullPath } from '../../utils/path';

class LPListPage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="w3-container">
                <BreadcrumbWithPath path={getfullPath(this.props)} />
                <LPListTable />
            </div>
        );
    }
}

LPListPage.propTypes = {

};

export default LPListPage;