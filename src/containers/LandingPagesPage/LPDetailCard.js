import { Card } from 'antd';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import React from 'react';

const LPDetailCard = (props) => {

    const landingPage  = props.landingPage || {};
    let link;
    if (Boolean(landingPage.product_gid)) {
        link = <Link to={"/pricebook/landing-pages/" + landingPage.course_id}>  Go to course information  </Link>;
    } else {
        link = '---';
    }

    // const convertCode = (code) => {
    //     code = code.replace("_", ' ');
    //     return code.charAt(0).toUpperCase() + code.slice(1);
    // };


    return (
        <Card className="card" title={landingPage.ga_code || landingPage.domain || landingPage.name || '---'} >
            <table>
                <tbody>
                    <tr>
                        <td>Alias Name</td>
                        <td>{landingPage.alias_name || '---'}</td>
                    </tr>
                    <tr>
                        <td>Course</td>
                        <td>
                            {link}
                        </td>
                    </tr>
                    <tr>
                        <td>Facebook page</td>
                        <td>{landingPage.facebook_page_gid || '---'}</td>
                    </tr>
                    <tr>
                        <td>Created at</td>
                        <td>{landingPage.created_at || '---'}</td>
                    </tr>
                    <tr>
                        <td>Updated at</td>
                        <td>{landingPage.updated_at || '---'}</td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>{landingPage.company_id || '---'}</td>
                    </tr>
                    <tr>
                        <td>Department</td>
                        <td>{landingPage.department_id || '---'}</td>
                    </tr>
                </tbody>
            </table>
        </Card>
    );
};


LPDetailCard.propsTypes = {
    landingPage: PropTypes.object.isRequired
};

export default LPDetailCard;