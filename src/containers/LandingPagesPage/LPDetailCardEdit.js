import { Button, Card, Form, Input, Select, Spin } from 'antd';
import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { simpleNotify } from '../../components/Alert';
import { updateLandingPage } from '../../actions/landingpage';

const FormItem = Form.Item;
const Option = Select.Option;

class LandingPageEditForm extends Component {
    constructor(props) {
        super(props);
        this._handleSubmit.bind(this);

        this.formItemLayout = {
            labelCol: {
                xs: 24, sm: 24, md: 8, lg: 8, xl: 8
            },
            wrapperCol: {
                xs: 24, sm: 24, md: 16, lg: 16, xl: 16
            },
        };

        this.tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 24,
                    offset: 0,
                },
                md: {
                    span: 16,
                    offset: 8,
                },
                lg: {
                    span: 16,
                    offset: 8,
                },
                xl: {
                    span: 16,
                    offset: 8,
                },
            },
        };

    }



    componentDidMount() {
        // if properties fetch data
    }
    _handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                Object.assign(values, { id: this.props.landingPage.id });
                this.props.updateLandingPage(values);
                if (!Boolean(this.props.landingPages.isRejected)) simpleNotify("Success", "Update sucessfully!");
            }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        const optionCourses = this.props.courses.map(e =>
            <Option key={e.id + '_course_option'} value={e.id}>{e.name}</Option>
        );

        const optionFacebookPage = this.props.facebookPages.map(e =>
            <Option key={e.id + '_facebookpage_option'} value={e.id}>{e.name}</Option>
        );
        return (
            <Card className="card" title="Edit information form">
                <Spin spinning={this.props.landingPages.isFetching}>
                    <Form onSubmit={this._handleSubmit} >
                        <div className="w3-container w3-padding-16">
                            <FormItem
                                {...this.formItemLayout}
                                label="Alias name"
                            >
                                {getFieldDecorator('alias_name', {
                                    rules: [{ required: true, message: 'Alias name...!' }],
                                    initialValue: this.props.landingPage.alias_name,
                                })(

                                    <Input placeholder="Alias name..." />
                                )}
                            </FormItem>
                            <FormItem
                                {...this.formItemLayout}
                                label="Course"
                            >
                                {getFieldDecorator('course_id', {
                                    rules: [{ required: true, message: 'Please select a course ...', type: 'number' }],
                                    initialValue: this.props.landingPage.course_id,
                                })(
                                    <Select
                                        showSearch
                                        placeholder="Select a course"
                                        optionFilterProp="children"
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    >
                                        {optionCourses}
                                    </Select>
                                )}
                            </FormItem>
                            <FormItem
                                {...this.formItemLayout}
                                label="Facebook page"
                            >
                                {getFieldDecorator('facebook_page_id', {
                                    rules: [{ required: true, message: 'Please select a facebook page...' }],
                                    initialValue: this.props.landingPage.facebook_page_id,
                                })(
                                    <Select
                                        showSearch
                                        placeholder="Select facebook page..."
                                        optionFilterProp="children"
                                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    >
                                        {optionFacebookPage}
                                    </Select>
                                )}
                            </FormItem>
                        </div>
                        <footer className="w3-container">
                            <FormItem {...this.tailFormItemLayout} >
                                <Button type="primary" htmlType="submit">Submit</Button>
                            </FormItem>
                        </footer>

                    </Form>
                </Spin>
            </Card>
        );
    }

}

LandingPageEditForm.propTypes = {
    landingPage: PropTypes.object.isRequired,
    courses: PropTypes.array.isRequired,
    facebookPages: PropTypes.array.isRequired,
    updateLandingPage: PropTypes.func.isRequired,
    landingPages: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        landingPages: state.landingPages
    };
};

const LPDetailCardEdit = Form.create()(LandingPageEditForm);

export default connect(mapStateToProps, { updateLandingPage })(LPDetailCardEdit);
