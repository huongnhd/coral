'use strict';

import { Button, Col, Input, Row } from 'antd';
import React, { Component } from 'react';

import CustomTable from '../../components/CustomTable';
import {Link} from 'react-router';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchLandingpages } from '../../actions/landingpage';

const Search = Input.Search;

class LPListTable extends Component {
    constructor(props) {
        super(props);

        _.bindAll(this, ['_onSearchButtonClick']);
        this.columns = this.columns = [
            {
                title: 'ID',
                dataIndex: 'id',
                key: 'id',
                sorter: true
            }, {
                title: 'Domain',
                dataIndex: 'domain_id',
                key: 'domain_id',
            }, {
                title: 'Alias name',
                dataIndex: 'alias_name',
                key: 'alias_name',
                render: (e => e || '---')
            }, {
                title: "Product",
                dataIndex: 'product_gid',
                key: 'product_gid',
                render: (e => e || '---')
            }, {
                title: "Facebook page",
                dataIndex: 'facebook_page_gid',
                key: 'facebook_page_gid',
                render: (e => e || '---')

            },
            {
                key: "tool",
                render: (record) => (
                    <Link to={"/pricebook/landing-pages/" + record.id}>
                        <Button
                            type="success"
                            size="small" >
                            show
                        </Button>
                    </Link>
                )
            }
        ];

    }

    componentWillMount() {

    }


    componentDidMount() {
        this.props.fetchLandingpages();
    }

    _onSearchButtonClick = (value) => {
        this.props.fetchLandingpages({ params: { full_search: value } });
    }

    render() {
        return (
            <div className="w3-container w3-card">
                <Row>
                    <Row style={{ padding: 10 }}>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                            <h3>Landing Pages Management </h3>
                        </Col>
                        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                            <Search
                                placeholder="input search text"
                                key="searchBox"
                                onSearch={this._onSearchButtonClick}
                                enterButton
                            />
                        </Col>
                    </Row>
                    <Row>
                        <CustomTable
                            className="w3-table-all w3-hoverable"
                            dataSource={this.props.landingPages.records}
                            key="landingPageTable"
                            columns={this.columns}
                            pagination={this.props.landingPages.pagination}
                            rowKey={(r) => r.id}
                            loading={this.props.landingPages.isFetching}
                            bordered

                        />
                    </Row>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { landingPages: state.landingPages };
};

LPListTable.propTypes = {
    fetchLandingpages: PropTypes.func.isRequired,
    landingPages: PropTypes.object.isRequired
};

export default connect(mapStateToProps, { fetchLandingpages })(LPListTable);