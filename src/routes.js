import { IndexRoute, Route } from 'react-router';
import {LPDetailPage, LPListPage} from './containers/LandingPagesPage';

import App from './containers/App/App';
import ConnectedCourseInfoPage from './containers/CoursesPage/CourseInfoPage';
import ConnectedCoursesPage from './containers/CoursesPage/CoursesPage';
import ConnectedUsersPage from './containers/UsersPage/UsersPage';
import HomePage from './containers/HomePage/HomePage';
import NotFoundPage from './components/NotFoundPage/NotFoundPage';
import React from 'react';

export default (
  <Route path="/" >    
    <IndexRoute component={HomePage}/>    
    <Route path="users" component={ConnectedUsersPage}/>     
    <Route path="/pricebook" component={App}>
      <IndexRoute component={HomePage}/>    
      <Route path="courses" component={ConnectedCoursesPage}/>
      <Route path="courses/:id" component={ConnectedCourseInfoPage}/>
      <Route path="landing-pages" component={LPListPage}/>
      <Route path="landing-pages/:id" component={LPDetailPage}/>
    </Route>
    <Route path="*" component={NotFoundPage}/>
  </Route>  
);