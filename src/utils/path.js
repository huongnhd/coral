/// Noted : this is props of component

const getfullPath = (props) => {
    const { pathname } = props.location;
    return pathname;

};

const getPathOrigin = (props) => {
    let { pathname } = props.location;
    if (pathname.endsWith('/')) {
        pathname = pathname.substring(0, pathname.length - 1);
    }
};

const concatPath = (props, path) => {
    let { pathname } = props.history.push(path);
    if (pathname.endsWith('/')) {
        pathname = pathname.substring(0, pathname.length - 1);
    }
};

export { getfullPath, getPathOrigin, concatPath };