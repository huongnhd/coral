import { COURSE_RATES, CREATE_COURSE_RATE, DELETE_COURSE_RATE, UPDATE_COURSE_RATE } from './../constants/actionTypes';

import { API_SOL } from './../config/api';
import { cFetch } from './../utils/cFetch';

export const fetchCourseRates = id => {
    const params = { course_id: id, fields: ['user_username', 'user'] };
    return {
        type: COURSE_RATES,
        payload: cFetch(API_SOL.course_rate, { method: "GET", params: params })
    };
};

export const createCourseRate = couresRate => {
    return {
        type: CREATE_COURSE_RATE,
        payload: cFetch(API_SOL.course_rate, { method: "POST", body: { course_rate: couresRate }, headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } })
    };
};


export const updateCourseRate = couresRate => {
    return {
        type: UPDATE_COURSE_RATE,
        payload: cFetch(API_SOL.course_rate + '/' + couresRate.id, { method: "PATCH", body: { course_rate: couresRate }, headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } })
    };
};


export const deleteCourseRate = id => {
    return {
        type: DELETE_COURSE_RATE,
        payload: cFetch(API_SOL.course_rate + '/' + id, { method: "DELETE" })
    };
};