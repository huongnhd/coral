import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import promiseMiddleware from './../utils/promise-middleware';
import nock from 'nock';
import { expect } from 'chai';
import {
  COURSES, COURSE_INFO
} from './../constants/actionTypes';
import { fetchCourses, fetchCourseInfo } from './course';
import { API_CONFIG } from './../config/api';

const middlewares = [thunk, promiseMiddleware()];
const mockStore = configureMockStore(middlewares);
const db = require('./../data/db.json');

describe('Course actions', function () {
  afterEach(() => {
    nock.cleanAll();
  });
  it('should create COURSES_SUCCESS when fetching courses has been done', () => {
    nock(API_CONFIG.host)
      .get((uri) => {
        return uri.indexOf(API_CONFIG.courses) >= 0;
      })
      .reply(200, db.products);
    const expectedActions = [
      { type: `${COURSES}_PENDING` },
      { type: `${COURSES}_FULFILLED`, payload: db.products }
    ];
    const store = mockStore();

    return store.dispatch(fetchCourses()).then(() => {
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

  });
});



describe(`${COURSE_INFO} actions get info`, () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it(`should create ${COURSE_INFO}_SUCCESS when fetching courses has been done`, () => {
    nock(API_CONFIG.host)
      .get((uri) => {
        return uri.indexOf(API_CONFIG.courses) >= 0;
      })
      .query(true, 921)
      .reply(200, db.products.records.filter(p => p.id = 921)[0]);
    const expectedActions = [
      { type: `${COURSE_INFO}_PENDING` },
      {
        type: `${COURSE_INFO}_FULFILLED`, payload: {
          "id": 921,
          "name": "Guitar đệm hát trong 30 ngày",
          "alias_name": "guitar-dem-hat-trong-30-ngay",
          "code": "HienNT.01",
          "source_id": "561251c4d8b731315500003b",
          "department_id": null,
          "created_at": "2016-11-05T16:59:00.000+07:00",
          "updated_at": "2017-07-28T23:23:22.000+07:00",
          "course_category_id": null,
          "category_name": "AC",
          "factor": 0.77,
          "marketing_start_date": null,
          "company_id": null,
          "user_id": null,
          "deleted_at": null
        }
      }
    ];
    const store = mockStore();

    return store.dispatch(fetchCourseInfo(921)).then(() => {
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

  });
});
