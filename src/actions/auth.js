import { API_AUTH } from './../config/api';
import {LOGIN} from './../constants/actionTypes';
import {cFetchV2} from './../utils/cFetch';
import cookie from 'js-cookie';

export const loginUser = (creds, cbk) => {
  return {
    type: LOGIN,
    fallback: cbk,
    payload: cFetchV2(API_AUTH.baseUri, API_AUTH.auth, { method: "POST", body: JSON.stringify(creds) }).then(response => {
      cookie.set('access_token', response.access_token);
    })
  };
};
