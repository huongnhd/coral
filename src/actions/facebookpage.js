'use strict';
/*  Calling API for CRUD action
 */

import {API_NAMI} from '../config/api';
import { LIST_FACEBOOK_PAGES } from '../constants/actionTypes';
import { cFetchV2 } from '../utils/cFetch';

/* params is 'search, order, paging'
 */
export const fetchFacebookpages = (params = {}) => {
    return {
        type: LIST_FACEBOOK_PAGES,
        payload: cFetchV2(API_NAMI.baseUri, API_NAMI.facebook_pages, { method: "GET", ...params })
    };
};
