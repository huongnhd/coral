import { COURSES, COURSES_CATEGORY, COURSE_INFO } from './../constants/actionTypes';

import { API_SOL } from './../config/api';
import {cFetch} from './../utils/cFetch';

export const fetchCourses = (params) => {
  // params = { page: 1, orders: [{ id: 'desc' }]
  if (params == null || params == undefined) {
    params = { page: 1, orders: [{ id: 'desc' }] };
  } else {
    if (params.page == null || params.page == undefined) {
      params.page = 1;
    }
    if (params.orders == null || params.orders == undefined) {
      params.orders = [{ id: 'desc' }];
    }

  }
  return {
    type: COURSES,
    payload: cFetch(API_SOL.courses, { method: "GET", ...params })
  };
};

export const fetchCourseInfo = id => {
  return {
    type: COURSE_INFO,
    payload: cFetch(API_SOL.courses + '/' + id, { method: "GET" })
  };
};

export const fetchCourseCategories = () => {
  return {
    type: COURSES_CATEGORY,
    payload: cFetch(API_SOL.course_categories, { method: "GET" })
  };
};