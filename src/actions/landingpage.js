'use strict';
/*  Calling API for CRUD action
 */

import { DETAIL_LANDING_PAGE, LIST_LANDING_PAGES, UPDATE_LANDING_PAGE } from '../constants/actionTypes';

import { API_HERA } from '../config/api';
import { cFetchV2 } from '../utils/cFetch';

/* params is 'search, order, paging'
 */
export const fetchLandingpages = (params = {}) => {
    return {
        type: LIST_LANDING_PAGES,
        payload: cFetchV2(API_HERA.baseUri, API_HERA.landing_pages, { method: "GET", ...params })
    };
};

export const fetchLandingPage = (id) => {
    return {
        type: DETAIL_LANDING_PAGE,
        payload: cFetchV2(API_HERA.baseUri, API_HERA.landing_pages + '/' + id)
    };
};


export const updateLandingPage = landingPage => {
    return {
        type: UPDATE_LANDING_PAGE ,
        payload: cFetchV2(API_HERA.baseUri, API_HERA.landing_pages + '/' + landingPage.id, { method: "PATCH",  body: { landing_page: landingPage }, headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } })
    };
};
