// Unify the default State
import cookie from 'js-cookie';

export default {
  auth: {
    isFetching: false,
    isAuthenticated: cookie.get('access_token') ? true : false
  },
  users: {
    isFetching: false,
    meta: {
      total: 0,
      perPage: 10,
      page: 1
    },
    data: []
  },
  courses: {
    isFetching: false,
    filters: {
      paging: {
        page: 1,
        per_page: 10,
        record_total: 0,
      }
    },
    records: []
  },
  course: {
    isFetching: false,
    record: {}
  },
  courseRates: {
    isFetching: false,
    records: [],
    record: {},
    newRecord: {}
  },
  landingPages: {
    isFetching: false,
    filters: {
      paging: {
        page: 1,
        per_page: 10,
        record_total: 0,
      }
    },
    records: [],
    record: {},

  },
  generalStructureData: {
    isFetching: false,
    filters: {
      paging: {
        page: 1,
        per_page: 10,
        record_total: 0,
      }
    },
    records: [],
    record: {},
    newRecord: {}
  }
};