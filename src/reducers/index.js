import {
  COURSES,
  COURSES_CATEGORY,
  COURSE_INFO,
  COURSE_RATES,
  CREATE_COURSE_RATE,
  DETAIL_LANDING_PAGE,
  LIST_FACEBOOK_PAGES,
  LIST_LANDING_PAGES,
  UPDATE_LANDING_PAGE,
  USER
} from '../constants/actionTypes';

import CourseRateReducer from './courseRateReducer';
import auth from './auth';
import { combineReducers } from 'redux';
import initialState from './initialState';
import reducersDetail from './reducersDetail';
import reducersGenerate from './reducersGenerate';
import reducersSimpleCRUD from './reducersSimpleCRUD';
import { routerReducer } from 'react-router-redux';

const users = reducersGenerate(USER, initialState.users);
const courses = reducersGenerate(COURSES, initialState.courses);
const course = reducersDetail(COURSE_INFO, initialState.course);

const courseCategories = reducersGenerate(COURSES_CATEGORY, initialState.generalStructureData);

const landingPages = reducersSimpleCRUD(
  [LIST_LANDING_PAGES, DETAIL_LANDING_PAGE, UPDATE_LANDING_PAGE], initialState.generalStructureData);

const facebookPages = reducersSimpleCRUD(
  [LIST_FACEBOOK_PAGES], initialState.generalStructureData);

const courseRates = CourseRateReducer([
  COURSE_RATES,
  CREATE_COURSE_RATE
], initialState.courseRates);

const rootReducer = combineReducers({
  routing: routerReducer,
  auth,
  users,
  courses,
  courseCategories,
  course,
  courseRates,
  landingPages,
  facebookPages
});

export default rootReducer;
