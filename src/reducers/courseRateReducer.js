import { COURSE_RATES, CREATE_COURSE_RATE, DELETE_COURSE_RATE, UPDATE_COURSE_RATE } from '../constants/actionTypes';

import _ from 'lodash';

export default function CourseRateReducer(actionType, initialState, optionsHandler) {
    if (!_.isArray(actionType)) {
        actionType = [actionType];
    }

    const defaultHandler = {};

    _.each(actionType, (item) => {
        // PENDING action
        defaultHandler[`${item}_PENDING`] = (state) => {
            return Object.assign({}, state, {
                isFetching: true
            });
        };

        // FULFILLED action
        defaultHandler[`${item}_FULFILLED`] = (state, action) => {
            switch (item) {
                case COURSE_RATES:
                    return {
                        isFulfilled: true,
                        isFetching: false,
                        records: action.payload.records,
                        filters: action.payload.filters
                    };
                case CREATE_COURSE_RATE:
                    {
                        state.records.unshift(action.payload);
                        return {
                            isFulfilled: true,
                            isFetching: false,
                            records: state.records,
                            filters: state.filters
                        };
                    }

                case UPDATE_COURSE_RATE:

                    {
                        const id = action.payload.id;
                        return {
                            isFulfilled: true,
                            isFetching: false,
                            records: state.records.map(r => {
                                if (r.id === id) Object.assign(r, action.payload)
                                return r;
                            }),
                            filters: state.filters
                        };
                    }
                case DELETE_COURSE_RATE:
                    return {
                        isFulfilled: true,
                        isFetching: false,
                        records: state.records,
                        filters: state.filters
                    };
                default: return state;
            }

        };

        // REJECTED action
        defaultHandler[`${item}_REJECTED`] = (state, action) => {
            return Object.assign({}, state, {
                isFetching: false,
                isRejected: true,
                error: action.payload
            });
        };
    });

    const actionHanlder = Object.assign({}, defaultHandler, optionsHandler);

    return (state = initialState, action) => {
        return actionHanlder[action.type] && actionHanlder[action.type](state, action) || state;
    };
}
