'use strict';

import _ from 'lodash';

const pagination = (filters) => {
    return {
        showSizeChanger: true,
        total: filters.record_total,
        pageSize: filters.per_page,
        pageSizeOptions: ['1', '10', '20', '40']
    };
};
/* Define a producer adaptive with common api of EDM marketing*/
export default function reducersSimpleCRUD(actionType, initialState, optionsHandler) {
    if (!_.isArray(actionType)) {
        actionType = [actionType];
    }

    const defaultHandler = {};

    _.each(actionType, (item) => {
        // PENDING action
        defaultHandler[`${item}_PENDING`] = (state) => {
            return Object.assign({}, state, {
                isFetching: true
            });
        };

        // FULFILLED action
        defaultHandler[`${item}_FULFILLED`] = (state, action) => {
            /* Get type of request CREATE, UPDATE, DELETE, LIST */
            let method = item.split('_')[0] || '';
            switch (method) {
                case 'LIST':
                    return {
                        isFulfilled: true,
                        isFetching: false,
                        records: action.payload.records,
                        filters: pagination(action.payload.filters)
                    };
                case 'CREATE':
                    {
                        state.records.unshift(action.payload);
                        return {
                            isFulfilled: true,
                            isFetching: false,
                            records: state.records,
                            filters: state.filters
                        };
                    }
                case 'DETAIL':
                    {
                        return {
                            isFulfilled: true,
                            isFetching: false,
                            record: action.payload
                        };
                    }

                case 'UPDATE':
                    return {
                        isFulfilled: true,
                        isFetching: false,
                        record: action.payload,
                        records: state.records,
                        filters: state.filters
                    };
                    
                case 'DELETE':
                    return {
                        isFulfilled: true,
                        isFetching: false,
                        records: state.records,
                        filters: state.filters
                    };
                default: console.error("Cant handle lib when request FULFILLED . Because cant determine action" + item);
            }

        };

        // REJECTED action
        defaultHandler[`${item}_REJECTED`] = (state, action) => {
            return Object.assign({}, state, {
                isFetching: false,
                isRejected: true,
                error: action.payload
            });
        };
    });

    const actionHanlder = Object.assign({}, defaultHandler, optionsHandler);

    return (state = initialState, action) => {
        return actionHanlder[action.type] && actionHanlder[action.type](state, action) || state;
    };
}
